(function($) {
      Drupal.behaviors.fuseiq_mobile_menu = {
        attach: function (context, settings) {
          if (context == document) {
            $(document).ready(function () {
              $('.fuseiq-mobile-menu').meanmenu(
                {
                  meanScreenWidth: "767",
                  meanMenuContainer: '.fuseiq-mobile-menu-container',
                  meanDisplay: "inline-block"
                }
            );
            });
          }
        }
      };
      
      
	
})(jQuery);