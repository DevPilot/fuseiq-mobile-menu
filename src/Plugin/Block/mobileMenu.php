<?php

namespace Drupal\fuseiq_mobile_menu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Component\Plugin\PluginBase;
/**
 * Provides a 'Mobile Menu' Block.
 *
 * @Block(
 *   id = "fuseiq_mobile_menu",
 *   admin_label = @Translation("Mobile Menu"),
 * )
 */
class mobileMenu extends BlockBase {

  /**
   * {@inheritdoc}
   */
     
  public function build() {
    
    $menu_name = 'main';
    
    $menu_parameters = new \Drupal\Core\Menu\MenuTreeParameters();
    $menu_parameters->setMaxDepth(2);
    
        $manipulators = array(
        // Only show links that are accessible for the current user.
        array('callable' => 'menu.default_tree_manipulators:checkAccess'),
        // Use the default sorting of menu links.
        array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
    );
    
    $menu_tree_service = \Drupal::service('menu.link_tree');
    $tree = $menu_tree_service->load($menu_name, $menu_parameters);
    $tree = $menu_tree_service->transform($tree, $manipulators);
    $menu = $menu_tree_service->build($tree);
    //$menu['#theme'] = 'menu_mobile';
    //ksm(\Drupal::service('renderer')->render($menu[0]));
	  
	  $output = array(
      '#items' => $menu['#items'],
      '#theme' => 'menu_mobile',
		  '#attached' => array(
      	'library' => array(
        	'fuseiq_mobile_menu/fuseiq-mobile-menu',
        ),
      ), 
 
	  );
	  	  
	 return $output;

    }
}
 